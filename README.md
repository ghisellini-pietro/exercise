# hello device driver

This is a device driver for exercise purpose.

It has to behave like this
```
$ echo "Hello!" > /dev/hello
$ cat /dev/hello
Hello!
```

## makefile
the makefile can build/clean/load/unload the module
```
sudo make
sudo make clean
sudo make load
sudo make unload
```

## how to use
- build with ```sudo make```
- insert module with ```sudo make load```
- get device number of hello driver with ```cat /proc/devices```
- make device usable, replace 241 with the number you get from previous step
    ```
    sudo mknod /dev/hello c 241 0
    sudo chmod 666 /dev/hello
    ```
- device driver is ready to be used

use ```sudo make unload``` once done

tested on:
Ubuntu 19.10
kernel: 5.3.0-29-generic