#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/module.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include<linux/slab.h>


MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Pietro Ghisellini");

static char *text;

/*
   Performs read from device driver 
*/
static ssize_t device_file_read(struct file *file_ptr, char __user *user_buffer, size_t count, loff_t *position)
{
   // check if device is empty
   if(!text)
   {
      printk("hello-driver: Device file is empty\n");
      return 0;
   }
   printk("hello-driver: Device file is read at offset = %i, read bytes count = %u\n", (int)*position, (unsigned int)count);

   // end of device driver reached
   if(*position >= sizeof(text))
   {
      printk("hello-driver: end reached\n");
      return 0;
   }

   if(*position + count > sizeof(text))
      count = sizeof(text) - *position;

   // copy to user
   if(raw_copy_to_user(user_buffer, text + *position, count) != 0)
   {
      printk("hello-driver: Failed to raw_copy data\n");
      return -EFAULT;   
   }
   printk("hello-driver: Data copied\n");
   
   *position += count;
   // return bytes in buffer
   return count;
}

/*
   Performs write to device driver
*/
static ssize_t device_file_write(struct file *file_ptr, const char __user *user_buffer, size_t count, loff_t *position)
{
   printk("hello-driver: Device file write at offset = %i, write bytes count = %u\n", (int)*position, (unsigned int)count);

   // allocate memory for input string
   // free memory on each next call
   if(text)
   {
      printk("hello-driver: free memory\n");
      kfree(text);
   }
   text = kmalloc(count, GFP_KERNEL);
   if(!text)
   {
      printk("hello-driver: Failed to allocate memory\n");
      return -EFAULT;
   }

   printk("hello-driver: Memory allocated\n");

   // copy from user
   if(raw_copy_from_user(text, user_buffer, count) != 0)
   {
      printk("hello-driver: Failed to copy data\n");
      return -EFAULT;
   }

   printk("hello-driver: Data copied\n");
   return count;
}

//
// device driver initialization section
//

// specify device driver operations
static struct file_operations simple_driver_fops = 
{
   .owner = THIS_MODULE,
   .read = device_file_read,
   .write = device_file_write
};

static int device_file_major_number = 0;
static const char device_name[] = "hello";

int register_device(void)
{
      int result = 0;

      printk("hello-driver: register_device() is called.\n");

      result = register_chrdev(0, device_name, &simple_driver_fops);
      if(result < 0)
      {
         printk("hello-driver: can\'t register character device with errorcode = %i\n", result);
         return result;
      }

      device_file_major_number = result;
      printk("hello-driver: registered character device with major number = %i and minor numbers 0...255\n", device_file_major_number);

      return 0;
}

void unregister_device(void)
{
   printk("hello-driver: unregister_device() is called\n");
   if(device_file_major_number != 0)
   {
      unregister_chrdev(device_file_major_number, device_name);
   }
}

static int simple_driver_init(void)
{
   int result = 0;
   printk("hello-driver: Initialization started\n");

   result = register_device();
   return result;
}

static void simple_driver_exit(void)
{
   printk("hello-driver: Exiting\n");
   unregister_device();
}

module_init(simple_driver_init);
module_exit(simple_driver_exit);